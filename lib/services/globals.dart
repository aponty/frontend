library spot_catch.globals;
import '../services/routes.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

bool isLoggedIn = false;
int userID;
String currentRoute = HomeRoute;
final navigatorKey = GlobalKey<NavigatorState>();
const bool isProduction = bool.fromEnvironment('dart.vm.product');
final bool isAndroid = Platform.isAndroid;
