import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:spot_catch/services/api/auth/auth_helpers.dart';
import 'package:spot_catch/services/api/endpoints.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:spot_catch/models/user.dart';

class UserConfigHelpers {
  static Future<Map> location() async {
    try {
      LocationData currentLocation;
      currentLocation = await Location().getLocation();
      Map data = {
        'last_latitude' : currentLocation.latitude.toStringAsPrecision(10),
        'last_longitude' : currentLocation.longitude.toStringAsPrecision(10),
      };
      return data;
    } catch(PlatformException) {
      await _showDialog("This app requires user location during use for basic functionality. Please enable in Settings and try again");
      await AuthHelpers.deleteToken();
    }
    return null;
  }

  static Future<dynamic> _showDialog(String text) async {
    BuildContext context = globals.navigatorKey?.currentState?.overlay?.context;
    if (context != null) {
      return showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title : Text(
            text,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }

  static Future<String> registerFCMNotificationToken({homeCallback}) async {
    // should to set onTokenRefresh as well here- get auth headers, update token in db
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        String currentRoute = globals.currentRoute;
        if (currentRoute == HomeRoute) {
          homeCallback(message);
        }
        if (currentRoute == MatchesRoute) {
          // This is a hack. refreshes screen to surface notifications. Can't get a callback to this, only config in one place
          // need to set up some real state management somehow and the move these globals into it
          Map args = {"disable_animation": true};
          globals.navigatorKey.currentState.pushReplacementNamed(MatchesRoute, arguments: args);
        } 
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _appClosedNotificationHandler(message, 'launch');
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _appClosedNotificationHandler(message, 'resume');
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true, provisional: false),
    );
    _firebaseMessaging.onIosSettingsRegistered.listen(
      (IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });

    String token = await _firebaseMessaging.getToken();
    return token;
  }

  static void _appClosedNotificationHandler(message, source) async {
    // clear the nav stack in case they've opened a few notifications, no-op in onLaunch
    globals.navigatorKey?.currentState?.popUntil((route) => route.settings.name == HomeRoute);
    message['data'] ??= {};
    String notificationType = message['data']['type_key'];
    bool isMessage = notificationType == 'message';
    int senderID = int.parse(message['data']['sender_id']);
    User match = await User.fetchMatch(senderID);

    if(isMessage) {
      globals.navigatorKey.currentState.pushNamed(ChatRoute, arguments: {'match': match });
    } else {
      globals.navigatorKey.currentState.pushNamed(MatchRoute, arguments: {'match' : match});
    }
  }

  static Future<void> postConfigData({homeCallback}) async {
    String id = globals.userID.toString();
    Map data = await location() ?? {};
    String token = await registerFCMNotificationToken(homeCallback: homeCallback);
    data['notification_token'] = token;
    await HTTPMethods.upload(data, "$USERS_ROOT$id", 'patch');
  }
}
