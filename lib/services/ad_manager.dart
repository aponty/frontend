import 'dart:io';
import 'dart:io' show Platform;
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:spot_catch/services/globals.dart' as globals;

class AdManager {
  static BannerAd _bannerAd;
  static InterstitialAd _interstitialAd;
  static const isProduction = globals.isProduction;

  static void initialize() {
    FirebaseAdMob.instance.initialize(appId: appId);
    _loadBanner();
    _loadInterstitial();
  }

  static void showBannerAd(double navBarHeight, BuildContext context) {
    double offset;
    double safePaddingTop = MediaQuery.of(context).padding.top;
    if (globals.isAndroid) {
      navBarHeight ??= 200.0;
      offset = safePaddingTop + navBarHeight + 2;
    } else {
      offset = safePaddingTop/4 + navBarHeight;
    }

    _bannerAd
      ..load()
      ..show(anchorOffset: offset, anchorType: AnchorType.top);
    Future.delayed(Duration(seconds: 5), () => _hideBannerAd());
  }

  static void showInterstitialAd() {
    _interstitialAd
      ..load()
      ..show();
    Future.delayed(Duration(seconds: 5), () => _hideInterstitialAd());
  }

  static final MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>['climbing', 'rock climbing', 'bouldering', 'outdoor activities', 'hiking', 'camping', 'trad climbing', 'climbing gym'],
    contentUrl: 'https://www.rei.com/',
    childDirected: false,
  );

  static void _loadBanner() {
    _bannerAd = homeBanner..load();
  }

  static void _hideBannerAd() async {
    await _bannerAd.dispose();
    _loadBanner();
  }

  static void _loadInterstitial() {
    _interstitialAd = interstitialAd..load();
  }

   static void _hideInterstitialAd() async {
    await _interstitialAd.dispose();
    _loadInterstitial();
  }



  static BannerAd get homeBanner {
    return BannerAd(
      adUnitId: bannerAdUnitId,
      size: AdSize.smartBanner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("BannerAd event is $event");
      },
    );
  }

  static InterstitialAd get interstitialAd {
    return InterstitialAd(
      adUnitId: interstitialAdUnitId,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("InterstitialAd event is $event");
      },
    );
  }

  static String get appId {
    if (Platform.isAndroid) {
      return "ca-app-pub-2288758359549053~3270264319";
    } else if (Platform.isIOS) {
      return "ca-app-pub-2288758359549053~1268941028";
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }

  static String get bannerAdUnitId {
    if (!isProduction) return BannerAd.testAdUnitId;
    print('WARNING: using prod ad unit ids! Using these in dev can result in an admob ban');
    if (Platform.isAndroid) {
      return "ca-app-pub-2288758359549053/2775440295";
    } else if (Platform.isIOS) {
      return "ca-app-pub-2288758359549053/9907824852";
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }

  static String get interstitialAdUnitId {
    if (!isProduction) return InterstitialAd.testAdUnitId;
    print('WARNING: using prod ad unit ids! Using these in dev can result in an admob ban');
    if (Platform.isAndroid) {
      return "ca-app-pub-2288758359549053/1270786937";
    } else if (Platform.isIOS) {
      return "ca-app-pub-2288758359549053/8403171493";
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }
}
