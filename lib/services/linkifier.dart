import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/gestures.dart';

class Linkifier extends StatelessWidget {
  static final urlPattern = r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?";
  static final String emailPattern = r'\S+@\S+';
  static final String phonePattern = r'[\d-]{9,}';
  static final RegExp linkRegExp = RegExp('($urlPattern)|($emailPattern)|($phonePattern)', caseSensitive: false);
  final String text;
  Linkifier(this.text);

  SelectableText buildTextWithLinks(String textToLink) => SelectableText.rich(TextSpan(children: linkify(textToLink)));

  static Widget textWithTrailingLink(String text, String textForLink, String url) {
    return InkWell(
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: text,
          style: TextStyle(
            color: Colors.black,
            decorationColor: Colors.black,
            fontSize: 15,
          ),
          children: [
            TextSpan(
              text: textForLink,
              style: TextStyle(
                color: Colors.blueAccent, 
                decoration: TextDecoration.underline,
              ),
            ),
          ]
        ),
      ),
      onTap: () => Linkifier.openUrl(url),
    );
  }

  static Future<void> openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  TextSpan buildLinkComponent(String text, String linkToOpen) => TextSpan(
    text: text,
    style: TextStyle(
      color: Colors.blueAccent,
      decoration: TextDecoration.underline,
    ),
    recognizer: TapGestureRecognizer()
      ..onTap = () => openUrl(linkToOpen),
  );

  List<InlineSpan> linkify(String text) {
    final List<InlineSpan> list = <InlineSpan>[];
    final RegExpMatch match = linkRegExp.firstMatch(text);
    if (match == null) {
      list.add(TextSpan(text: text));
      return list;
    }

    if (match.start > 0) {
      list.add(TextSpan(text: text.substring(0, match.start)));
    }

    final String linkText = match.group(0);
    if (linkText.contains(RegExp(urlPattern, caseSensitive: false))) {
      list.add(buildLinkComponent(linkText, linkText));
    }
    else if (linkText.contains(RegExp(emailPattern, caseSensitive: false))) {
      list.add(buildLinkComponent(linkText, 'mailto:$linkText'));
    }
    else if (linkText.contains(RegExp(phonePattern, caseSensitive: false))) {
      list.add(buildLinkComponent(linkText, 'tel:$linkText'));
    } else {
      throw 'Unexpected match: $linkText';
    }

    list.addAll(linkify(text.substring(match.start + linkText.length)));

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return buildTextWithLinks(text);
  }
}
