import '../../services/api/auth/auth_helpers.dart';
import 'package:http/http.dart' as http;
import '../routes.dart';

class HTTPMethods {
  static Future<bool> delete(endpoint) async {
    final Map<String, String> authHeader = await AuthHelpers.authTokenHeader();
    String url = Router.appRoute() + endpoint;
    try {
      http.Response res = await http.delete(url, headers: authHeader);
      print(res);
      if (res.statusCode == 200) {
        return true;
      } else {
        print("failed to delete for DELETE on $endpoint");
        print(res.statusCode);
        print(res);
        return false;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future upload(data, route, method) async {
    final Map<String, String> authHeader = await AuthHelpers.authTokenHeader();
    Map<String, Function> methods = {
      'patch': http.patch,
      'post': http.post,
    };
    String url = Router.appRoute() + route;
    try {
      http.Response res = await methods[method](url, body: data, headers: authHeader);
      if (res.statusCode == 201 || res.statusCode == 200) {
        return res;
      } else if (res.statusCode == 401) {
        AuthHelpers.deleteToken();
        print('unauthorized request: $method on $route');
        print(res.statusCode);
        print(res.body);
        return res;
      } else {
        print('failed to create or update: $method on $route');
        print(res.statusCode);
        print(res.body);
        return res;
      }
    } catch (e) {
      print(e);
      return e;
    }
  }

  static fetch(route) async {    
    final Map<String, String> authHeader = await AuthHelpers.authTokenHeader();
    String url = Router.appRoute() + route;
    try {
      http.Response res = await http.get(url, headers: authHeader);
      if (res.statusCode == 200) {
        return res;
      } else if (res.statusCode == 401) {
        AuthHelpers.deleteToken();
        print('unauthorized request: GET on $route');
        print(res.statusCode);
        print(res.body);
        return res;
      } else {
        print('failed to fetch: GET on $route');
        print(res.statusCode);
        print(res.body);
        return res;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
