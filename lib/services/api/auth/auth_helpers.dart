import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:spot_catch/services/api/endpoints.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/services/routes.dart';

class AuthHelpers {
  static Future<bool> setLocalTokens(response) async {
    final storage = new FlutterSecureStorage();
    var body = json.decode(response.body);

    int id = body['data']['id'];
    var uid = response.headers['uid'];
    var accessToken = response.headers['access-token'];
    var client = response.headers['client'];

    await Future.wait([
      storage.write(key: 'userID', value: id.toString()),
      storage.write(key: 'uid', value: uid),
      storage.write(key: 'access-token', value: accessToken),
      storage.write(key: 'client', value: client),
    ]);

    globals.isLoggedIn = true;
    globals.userID = id;
    return true;
  }

  static Future<Map<String, String>> authTokenHeader() async {
    final storage = new FlutterSecureStorage();

    List vals = await Future.wait([
      storage.read(key: 'uid'),
      storage.read(key: 'access-token'),
      storage.read(key: 'client'),
    ]);
    
    String uid = vals[0];
    String access = vals[1];
    String client = vals[2];
    return {
      'uid': uid,
      'access-token': access,
      'client': client,
    };
  }

  static Future<bool> hasToken() async {
    // also sets global id var
    final storage = new FlutterSecureStorage();

    List vals = await Future.wait([
      storage.read(key: 'userID'),
      storage.read(key: 'uid'),
      storage.read(key: 'access-token'),
      storage.read(key: 'client'),
    ]);

    String id = vals[0];

    final idRes = id != null;
    final uidRes = vals[1] != null;
    final accessTokenRes = vals[2] != null;
    final clientRes = vals[3] != null;

    bool hasToken = idRes && uidRes && accessTokenRes && clientRes;
    if (hasToken) globals.userID = int.parse(id);
    return hasToken;
  }

  static Future<void> deleteToken() async {
    final storage = new FlutterSecureStorage();
    await storage.deleteAll();
    globals.isLoggedIn = false;
  }

  static Future<dynamic> requestLogin( context, email, password, toggleLogin) async {
    Map<String, String> data = {
      'email': email,
      'password': password,
    };
    http.Response res = await HTTPMethods.upload(data, '/auth/sign_in', 'post');
    var body = json.decode(res.body);

    if (res?.statusCode == 200) {
      await AuthHelpers.setLocalTokens(res);
      FocusScope.of(context).unfocus();
      Navigator.pop(context);
    } else if (res?.statusCode == 401) {
      AuthHelpers.deleteToken();
      showSnackbar(context, 'Invalid credentials. Please sign up, try again, or reset your password.');
    } else {
      AuthHelpers.deleteToken();
      String status = res?.statusCode?.toString() ?? "Our server's napping... please email spot.catch.dev@gmail.com";
      String errors = body['errors'].first ?? '';
      String message = body['message'] ?? '';
      showSnackbar(context, "Hmm. Request failed: $status $errors $message");
    }
  }

  static void showSnackbar(context, text) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 5),
        content: Text(text),
      ),
    );
  }

  static Future<dynamic> requestSignUp(context, data, toggleLogin) async {
    http.Response res = await HTTPMethods.upload(data, '/auth', 'post');

    if (res?.statusCode == 200) {
      await AuthHelpers.setLocalTokens(res);
      FocusScope.of(context).unfocus();
      Navigator.of(context).popAndPushNamed(HomeRoute);
      Navigator.of(context).pushNamed(AccountRoute);
      Navigator.of(context).pushNamed(ProfileRoute);
    } else if (res?.statusCode == 422) {
      showSnackbar(context, "This email address already has an account. Please log in.");
      toggleLogin();
    } else {
      String status = res?.statusCode?.toString();
      status ??= "Our server might be down... please email spot.catch.dev@gmail.com and then make the atmosphere hasn't turned to flames or something";
      showSnackbar(context, "Request failed with status: $status. Please try again later.");
    }
  }

  static Future<dynamic> requestReset(context, email) async {
    AuthHelpers.deleteToken();
    Map<String, String> data = {'email': email};
    http.Response res = await HTTPMethods.upload(data, RESET_PASSWORD, 'post');
    var body = json.decode(res.body);
    print(body);

    if (res?.statusCode == 200) {
      showSnackbar(context, body['message']);
    } else if (res?.statusCode == 404) {
      showSnackbar(context, body['errors'].first);
    } else {
      String status = res?.statusCode?.toString();
      status ??= "Our server's napping... please email spot.catch.dev@gmail.com";
      String errors = body['errors'].first;
      errors ??= '';
      String message = body['message'];
      message ??= '';
      showSnackbar(context, "Hmm. Request failed: $status $errors $message");
    }
  }

  static Future<void> requestLogout(context) async {
    try {
      await HTTPMethods.delete(LOGOUT);
      await AuthHelpers.deleteToken();
      Navigator.pushNamedAndRemoveUntil(context, HomeRoute, (r) => false);
    } catch (e) {
      await AuthHelpers.deleteToken();
      showSnackbar(context, e);
    }
  }
}
