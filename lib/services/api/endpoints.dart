// this doesn't work super well with dynamic (/id/) urls. Make methods that return those, maybe? At least it would put things in one place...
// move the AppRoute over here as well whenever you get around to that
const LOGIN = '/auth/sign_in';
const LOGOUT = '/auth/sign_out';
const IMAGES_ROOT = '/images';
const PROFILE_IMAGES = '/images/profile_images';
const MATCHES_ROOT = "/matches";
const MATCHES_DISLIKE = "/matches/dislike";
const RESET_PASSWORD = '/auth/password';
const REPORTS_ROOT = '/reports';
const USERS_ROOT = '/users/';
const NOTIFICATIONS = '/notifications';
const ERROR_LOG = '/error_log';
