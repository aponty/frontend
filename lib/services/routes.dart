import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spot_catch/screens/account_screen/account_screen.dart';
import 'package:spot_catch/screens/account_screen/profile_form_screen.dart';
import 'package:spot_catch/screens/auth_screen/auth_screen.dart';
import 'package:spot_catch/screens/home_screen/home_screen.dart';
import 'package:spot_catch/screens/auth_screen/password_reset_screen.dart';
import 'package:spot_catch/screens/matches_screen/matches_screen.dart';
import 'package:spot_catch/screens/chat_screen/chat_screen.dart';
import 'package:spot_catch/screens/match_screen/match_screen.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/screens/legal_screen.dart';

// in-app routes
const HomeRoute = '/';
const AuthRoute = '/auth';
const ProfileRoute = '/profile';
const AccountRoute = '/account';
const MatchRoute = '/match';
const MatchesRoute = '/matches';
const ChatRoute = '/chat';
const PasswordResetRoute = '/password_reset';
const LegalRoute = '/legal';

class Router {
  static String appRoute() {
    if (globals.isProduction) return "https://spotcatch.co";
    if (globals.isAndroid) return "http://10.0.2.2:3000";
    return "http://localhost:3000";
  }

  static String chatSocketRoute() {
    if (globals.isProduction) return "wss://spotcatch.co/cable";
    if (globals.isAndroid) return 'ws://10.0.2.2:3000/cable';
    return "ws://localhost:3000/cable";
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    // all arguments passed to Navigator.pushNamed(context, targetRoute, arguments: {<right here>});
    // are accessable/passable to a new route here
    String route = settings.name;
    Map<dynamic, dynamic> data = settings.arguments ?? {};
    bool disableAnimations = data['disable_animation'];
    disableAnimations ??= false;

    switch (route) {
      case HomeRoute:
        globals.currentRoute = HomeRoute;
        if (disableAnimations) return _noAnimateRoute(settings, HomeScreen());
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => HomeScreen(),
        );
      case AccountRoute:
        globals.currentRoute = AccountRoute;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => AccountScreen(),
        );
      case AuthRoute:
        globals.currentRoute = AuthRoute;
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => AuthScreen(),
        );
      case ProfileRoute:
        globals.currentRoute = ProfileRoute;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ProfileScreen(),
        );
      case PasswordResetRoute:
        globals.currentRoute = PasswordResetRoute;
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => PasswordResetScreen(),
          );
      case MatchRoute:
        globals.currentRoute = MatchRoute;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MatchScreen(data['match']),
        );
      case MatchesRoute:
        globals.currentRoute = MatchesRoute;
        if (disableAnimations) return _noAnimateRoute(settings, MatchesScreen());
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => MatchesScreen(),
        );
      case ChatRoute:
        globals.currentRoute = ChatRoute;
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ChatScreen(data['match']),
        );
      case LegalRoute:
        globals.currentRoute = LegalRoute;
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => LegalScreen(),
        );
      default:
        globals.currentRoute = settings.name;
        return CupertinoPageRoute(
          settings: settings,
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ),
        );
    }
  }

  static PageRoute _noAnimateRoute(settings, screen) {
    return PageRouteBuilder(
      settings: settings,
      pageBuilder: (_, __, ___) => screen,
      transitionDuration: Duration(seconds: 0),
    );
  }
}
