import 'package:flutter/material.dart';
import 'package:spot_catch/services/linkifier.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';

class LegalScreen extends StatelessWidget {
  final String termsAndConditions = "There is no tolerance for objectionable content or abusive users. If you encounter either, please block and report them. All users can unmatch (block) any other user that they are in communication with at any time, ending all contact and removing all shared content.";
  ImageIcon logo(){
    return ImageIcon(
      AssetImage("assets/images/branding/v_lockup_trans.png"),
      color: Colors.black,
      size: 200,
    );
  }

  Widget header(String text) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: Text(
        text,
        style: TextStyle(
          color: Colors.black,
          decorationColor: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavBarHelpers.navBarBack(context),
      body: Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                logo(),
                header("Contact Us"),
                Linkifier.textWithTrailingLink(
                  "In the event of issues or questions not covered by the following Terms and Conditions, EULA, and Privacy Policy, please email ",
                  "spot.catch.dev@gmail.com",
                  "mailto:spot.catch.dev@gmail.com?subject=App%20User%20Content%20%28please%20leave%20subject%20as%20is%20for%20filtering%20purposes%29"
                ),
                header("Terms and Conditions of Usage"),
                Text(
                  termsAndConditions, 
                  style: TextStyle(
                    color: Colors.black,
                    decorationColor: Colors.black,
                    fontSize: 15,
                  ),
                ),
                header("End User License Agreement (EULA)"),
                Linkifier.textWithTrailingLink(
                  "All usage of this application is subject to our ",
                  'End User License Agreement (EULA)',
                  "https://www.apple.com/legal/internet-services/itunes/dev/stdeula/",
                ),
                header("Privacy Policy"),
                Linkifier.textWithTrailingLink(
                  "This app must collect user information (such as location, email, and profile information) and store user-generated content in order to operate. All content uploaded to our application is subject to the terms of our ",
                  "Privacy Policy",
                  "https://spotcatch.co/privacy",
                ),
                SizedBox(height: 50)
              ],
            ),
          ),
        ), 
      ),
    );
  }
}
