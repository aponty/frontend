import 'package:flutter/material.dart';
import '../../models/user.dart';
import '../partials/nav_bar/nav_bar.dart';
import '../../services/routes.dart';
import '../../services/globals.dart' as globals;
import '../../models/message.dart';

class MatchesScreen extends StatefulWidget {
  @override
  createState() => _MatchesScreenState();
}

class _MatchesScreenState extends State<MatchesScreen> {
  bool hasData = false;
  List<User> matches;

  @override
  void initState() {
    super.initState();
    if (globals.isLoggedIn) loadMatches();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void loadMatches() async {
    setState(() => hasData = false);
    List<User> allMatches = await User.fetchMatches();
    setState(() {
      matches = allMatches;
      hasData = true;
    });
    if (matches != null) sortMatches('recent');
  }

  void toggleRecentDistance(choice){
    if (choice == 'Most Recent') {
      sortMatches('recent');
    } else {
      sortMatches('closest');
    }  
  }

  _onUserTap(BuildContext context, User match) async {
    var args = {'match': match};
    await Navigator.pushNamed(context, ChatRoute, arguments: args);
    loadMatches();
  }

  Widget trailingIcon(BuildContext context, User match){
    Message lastMess = match.messages.last;
    bool isNewMatch = !match.userHasSeen;
    bool hasLastMess = !(lastMess.body == null);
    bool seenByUser = lastMess?.seenById(globals.userID);
    bool seenByMatch = lastMess?.seenById(match.userID);
    bool hasNewMessage = hasLastMess && !seenByUser;

    const noteDot = Icon(
      Icons.brightness_1,
      size: 20.0,
      color: Colors.redAccent,
    );

    const seenByOne = Icon(
      Icons.done,
      color: Colors.black,
      size: 20,
    );

    const seenByMany = Icon(
      Icons.done_all,
      color: Colors.black,
      size: 20,
    );

    if (isNewMatch || hasNewMessage) {
      return noteDot;
    } else if (seenByUser){
      return seenByOne;
    } else if (seenByUser && seenByMatch) {
      return seenByMany;
    }
    return null;
  }

  Widget listTile(User match, BuildContext context) {
    Widget leading = Padding(
      padding: EdgeInsets.only(left: 10.0),
      child: match.icon(30),
    );

    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: leading,
            title: Text(match.name ?? 'Name'),
            subtitle: Text(
              match.messages.first.body ?? '',
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () => _onUserTap(context, match),
            trailing: trailingIcon(context, match),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.black54),
              ),
            ),
            width: MediaQuery.of(context).size.width * 0.9,
          ),
        ],
      ),
    );
  }

  void mostRecent() {
    setState(() => hasData = false);
    matches.sort( (a, b) => b.recentDate().compareTo(a.recentDate()));
    setState(() {
      matches = matches;
      hasData = true;
    });
  }

  void closest() {
    setState(() => hasData = false);
    matches.sort((a, b) => a.distanceFrom.compareTo(b.distanceFrom));
    setState(() {
      matches = matches;
      hasData = true;
    });
  }

  void sortMatches(byMostRecent) {
    byMostRecent == 'recent' ? mostRecent() : closest();
  }

  @override
  Widget build(BuildContext context) {
    globals.currentRoute = MatchesRoute;
    return Scaffold(
      appBar: NavBarHelpers.navBarMatches(context, toggleRecentDistance),
      body: hasData ? 
        ListView(
          children: matches.map((match) => listTile(match, context)).toList(),
        ) 
        : Center(child: CircularProgressIndicator()),
    );
  }
}
