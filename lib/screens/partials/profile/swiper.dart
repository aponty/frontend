import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../../../models/user.dart';

class MySwiper extends StatelessWidget{
  final User match;
  final int itemCount;
  final bool beenReported;

  MySwiper(this.match, this.beenReported) :
    // -1 because the last image is an icon
    itemCount = (match.images.length - 1 > 1) ? match.images.length - 1 : 1;

  Widget reportedBanner(BuildContext context){
    Text reportText = Text(
      "mebbe tryna' date",
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        color: Colors.white, 
      ),
    );
    return Positioned(
      top: 0,
      child: Opacity(
        child: Container(
          height: 20,
          width: MediaQuery.of(context).size.width,
          color: Colors.redAccent,
          child: Center(child: reportText),
        ),
        opacity: 0.9,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double square = MediaQuery.of(context).size.width * 0.95;
    return Container(
      height: square,
      width: square,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        child: Swiper(
          itemBuilder: (context, index) {
            String imageKey = 'image_$index';
            if(match.images[imageKey]?.id != null) {
              return SingleChildScrollView(
                key: UniqueKey(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: match.images[imageKey].image,
                        ),
                        beenReported ? reportedBanner(context) : Container(),
                      ],
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  border: Border(
                    right: BorderSide(width: 1.0, color: Colors.black),
                  ),
                ),
                child: Center(
                  child: Text('No Image Available')
                )
              );
            }
          },
          itemCount: itemCount,
          viewportFraction: 1.0,
          loop: false,
        ),
      ),
    );
  }
}
