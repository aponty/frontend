import 'package:flutter/material.dart';
import 'package:spot_catch/models/user.dart';
import 'package:spot_catch/screens/partials/profile/swiper.dart';

class Profile extends StatefulWidget {
  final UniqueKey key;
  final User match;
  final double appBarHeight;
  final bool makeSpaceForLikeButtons;
  
  Profile(this.key, this.match, this.appBarHeight, {this.makeSpaceForLikeButtons});

  @override
  _ProfileState createState() => _ProfileState(key, match, appBarHeight, makeSpaceForLikeButtons);
}

class _ProfileState extends State<Profile> {
  final UniqueKey key;
  final User match;
  final double appBarHeight;
  bool makeSpaceForLikeButtons;
  bool descExpanded = true;

  _ProfileState(this.key, this.match, this.appBarHeight, makeSpaceForLikeButtons)
    : this.makeSpaceForLikeButtons = makeSpaceForLikeButtons ?? false;
  
  Widget _gradeItem(String count, String label) {
    TextStyle _statLabelTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 15.0,
      fontWeight: FontWeight.bold,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black87,
      fontSize: 11.0,
    );

    return Expanded(
      flex: 1,
      child: Row(
        children: [
          Spacer(),
          Text(label, style: _statLabelTextStyle),
          Spacer(),
          Text( count, style: _statCountTextStyle),
          Spacer(),
        ],
      )
    );
  }

  Widget _grades() {
    return Container(
      height: 40.0,
      width: MediaQuery.of(context).size.width * 0.35,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _gradeItem(match.boulderingGrade, "Bldr"),
              _gradeItem(match.sportGrade, "Sport"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _gradeItem(match.tradGrade, "Trad"),
              _gradeItem(match.trGrade, 'TR'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _name() {
    return FittedBox(
      fit: BoxFit.contain,
      child: Text(
        "${match.name}",
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      ),
    ); 
  }

  Widget _sketchRatingDisplay(){
    ImageIcon selected = ImageIcon(
      AssetImage("assets/images/chili_icon.png"),
      color: Colors.redAccent,
      size: 15,
    );
    ImageIcon unselected = ImageIcon(
      AssetImage("assets/images/chili_icon.png"),
      color: Colors.grey,
      size: 15,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(5, (index) => (index < match.sketchRating) ? selected : unselected),
    );
  }

  Widget _descriptionBody(){
    Text full = Text(
      match.description, 
      textAlign: TextAlign.left,
    );
    
    Text trunc = Text(
      match.description, 
      textAlign: TextAlign.left,
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
    );
    return descExpanded ? full : trunc;
  }

  Widget imageSwiper(){
    return Material( // with Material
      child: MySwiper(match, match.beenReported),
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
    );
  }

  double profileHeight(BuildContext context){
    double safePaddingBottom = MediaQuery.of(context).padding.bottom;
    double safePaddingTop = MediaQuery.of(context).padding.top;
    double spacer = makeSpaceForLikeButtons ? 100 : safePaddingBottom;
    double appHeight = MediaQuery.of(context).size.height;
    return appHeight - appBarHeight - safePaddingBottom - safePaddingTop - spacer;
  }

  double descriptionPosition(BuildContext context){
    double estimatedExpandedSize = 175; //should layout first and query/set actual size using postFrameCallBack but decent approximation
    double availableHeight = profileHeight(context);
    double imageHeight = MediaQuery.of(context).size.width * 0.95;
    final bool descOverlapsImg = estimatedExpandedSize + imageHeight > availableHeight;
    return descOverlapsImg ? 0 : availableHeight - (imageHeight + 5) - estimatedExpandedSize;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: profileHeight(context),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 5,
            child: imageSwiper(),
          ),
          Positioned(
            bottom: descriptionPosition(context),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.85,
              child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
                elevation: 4,
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: InkWell(
                    onTap: () => setState(() => descExpanded = !descExpanded),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.35,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  _name(),
                                  _sketchRatingDisplay(),
                                ],
                              ),
                            ),
                            _grades(),
                          ],
                        ),
                        _descriptionBody(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
