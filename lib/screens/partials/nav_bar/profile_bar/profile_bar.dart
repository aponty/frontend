import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/services/routes.dart';
import 'dart:math';

class ProfileBar with NavBarHelpers {
  static void loadingModal(context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Saving...', textAlign: TextAlign.center),
          content: SizedBox(
            height: 100,
            width: 100,
            child: Center(child:CircularProgressIndicator()),
          ),
        );
      },
    );
  }

  static void back(context) async {
    // fake loading modal for 0.5-2 seconds to make users comfortable with the ux
    NavBarHelpers.closeKeyboard(context);
    loadingModal(context);
    int randInt(int min, int max) => min + Random().nextInt((max + 1) - min);
    Future.delayed(Duration(milliseconds: randInt(500, 2000)), () {
      Navigator.popUntil(context, ModalRoute.withName(AccountRoute));
    });
  }

  static AppBar bar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 5,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(CupertinoIcons.left_chevron),
            onPressed: () => back(context),
          ),
          Text("Edit Profile"),
        ],
      ),
    );
  }
}


 