import 'package:flutter/material.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/services/routes.dart';

class ProfileSettings extends StatelessWidget with NavBarHelpers {

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: Icon(Icons.settings),
      onSelected: (String choice) {
        switch (choice) {
          case 'Contact':
            NavBarHelpers.contactUsDialogue(context);
            break;
          case 'Legal Information':
            Navigator.pushNamed(context, LegalRoute);
            break;
          case 'Log Out':
            NavBarHelpers.logoutConfirmDialogue(context);
            break;
        }
      },
      itemBuilder: (BuildContext context) {
        return {'Contact', 'Legal Information', 'Log Out'}.map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    );
  }

}
