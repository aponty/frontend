import 'package:flutter/material.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/models/user.dart';
import 'package:spot_catch/services/api/endpoints.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/services/routes.dart';

class UnmatchReport extends StatelessWidget {
  final BuildContext context;
  final User match;
  UnmatchReport(this.context, this.match);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (String choice) {
        switch (choice) {
          case 'Unmatch':
            unmatchConfirm(context);
            break;
          case 'Report':
            reportConfirm(context);
            break;
        }
      },
      itemBuilder: (BuildContext context) {
        return {'Unmatch', 'Report'}.map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    );
  }

  Future<void> _sendReport(BuildContext context, bool otherThan) async {
    var data = {
      'user_id': globals.userID.toString(),
      'report_id' : this.match.userID.toString(),
      'other_than_dating': otherThan.toString(),
    };
    await HTTPMethods.upload(data, REPORTS_ROOT, 'post');
    _navigateToMatches();
  }

  void _navigateToMatches(){
    globals.currentRoute = MatchesRoute;
    Navigator.popUntil(context, ModalRoute.withName(MatchesRoute));
  }

  Future<void> reportConfirm(BuildContext context) {
    Widget dateReport = RaisedButton(
      child: Text("Report for datin'"),
      onPressed: () => _sendReport(context, false),
    );

    Widget otherReport = RaisedButton(
      child: Text("Otherwise Inappropriate"),
      onPressed: () => _sendReport(context, true)
    );

    Widget cancel = RaisedButton(
      child: Text("Cancel"),
      onPressed: () => Navigator.of(context).pop(),
    );
    return NavBarHelpers.dialogue(context, 'Report & Unmatch', [dateReport, otherReport, cancel]);
  }

  Future<void> unmatchConfirm(BuildContext context) {
    String title = 'Are you sure?';
    var data = {
      'id': globals.userID.toString(),
      'match_user_id' : this.match.userID.toString(),
    };
    Widget confirm = RaisedButton(
      child: Text('Ok'),
      onPressed: () async {
        await HTTPMethods.upload(data, '/matches/deactivate', 'patch');
        _navigateToMatches();
      });
    Widget cancel = RaisedButton(
      child: Text("Cancel"),
      onPressed: ()=> Navigator.of(context).pop(),
    );
    return NavBarHelpers.dialogue(context, title, [confirm, cancel]);
  }

}
