import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/screens/partials/nav_bar/unmatch_report.dart';
import 'package:spot_catch/services/api/auth/auth_helpers.dart';

class NavBarHelpers {
  static AppBar navBarMain(BuildContext context, bool hasNotifications, Function initialize) {
    const redDot = Icon(
      Icons.brightness_1,
      size: 12.0,
      color: Colors.redAccent,
    );
    const Color color = Colors.black;
    return AppBar(
      elevation: 5,
      leading: IconButton(
        icon: Icon(
          Icons.account_circle,
          size: 22,
          color: color,
        ),
        tooltip: 'Profile and Settings',
        onPressed: _navigateMain(context, AccountRoute, initialize),
      ),
      title: IconButton(
        icon: ImageIcon(
          AssetImage("assets/images/branding/logomark_190_trans.png"),
          color: Colors.black,
          size: 300,
        ),
        onPressed: (){},
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: Stack(
            children:[
              Icon(
                Icons.chat,
                size: 22,
                color: color,
              ),
              Positioned(
                top: -1.0,
                right: -1.0,
                child: hasNotifications ? redDot : Container(),
              ),
            ]
          ),
          tooltip: 'Matches and Conversations',
          onPressed: _navigateMain(context, MatchesRoute, initialize),
        ),
      ],
    );
  }

  static AppBar navBarBack(BuildContext context) {
    return AppBar(
        elevation: 5,
        leading: backButton(context),
        title: logoIcon(),
        centerTitle: true,
        );
  }

  static AppBar navBarMatches(BuildContext context, toggleRecentDistance) {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 5,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          backButton(context),
          logoIcon(context: context),
          sortMatches(context, toggleRecentDistance),
        ],
      ),
    );
  }

  static AppBar navBarJustLogo() {
    return AppBar(
      elevation: 5,
      leading: Container(),
      title: logoIcon(),
      centerTitle: true,
    );
  }

  static AppBar navBarChat(BuildContext context, CircleAvatar matchAvatar, match) {
    return AppBar(
        elevation: 5,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            backButton(context),
            IconButton(
              icon: matchAvatar,
              onPressed: () => Navigator.pushNamed(context, MatchRoute,
                arguments: {'match': match}),
            ),
            UnmatchReport(context, match),
          ],
        ),
    );
  }

  static AppBar navBarMatch(BuildContext context, match) {
    return AppBar(
        elevation: 5,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            backButton(context),
            logoIcon(),
            UnmatchReport(context, match),
          ],
        ),
    );
  }

  static Widget sortMatches(context, toggleRecentDistance) {
    return PopupMenuButton(
      icon: Icon(Icons.filter_list) ,
      onSelected: (String choice) {
        toggleRecentDistance(choice);
      },
      itemBuilder: (BuildContext context) {
        return {'Most Recent', 'Closest'}.map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    );
  }

  static Widget logoIcon({context}) {
    Function onPressed = context == null ? (){} : () => Navigator.popUntil(context, ModalRoute.withName(HomeRoute));
    return IconButton(
      icon: ImageIcon(
        AssetImage("assets/images/branding/logomark_190_trans.png"),
        color: Colors.black,
        size: 300,
      ),
      onPressed: onPressed,
    );
  }

  static Widget backButton(BuildContext context) {
    return IconButton(
      icon: Icon(CupertinoIcons.left_chevron),
      onPressed: () => {
        closeKeyboard(context),
        Navigator.pop(context)
      },
    );
  }

  static Future<void> dialogue(BuildContext context, title, children) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(child: Text(title)),
          content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ...children
              ],
            ),
        );
      },
    );
  }

  static Future<void> logoutConfirmDialogue(BuildContext context) {
    Widget logout = RaisedButton(
      child: Text("Log Out"),
      onPressed: () => AuthHelpers.requestLogout(context),
    );

    Widget cancel = RaisedButton(
      child: Text("Cancel"),
      onPressed: () => Navigator.of(context).pop(),
    );
    String title = 'Are you sure you want to log out?';
    return NavBarHelpers.dialogue(context, title, [logout, cancel]);
  }

  static Future<void> contactUsDialogue(BuildContext context) {   
   Text info = Text(
     "If you're having issues, please email at spot.catch.dev@gmail.com",
      textAlign: TextAlign.center,
    );

    Widget close = RaisedButton(
      child: Text("Close"),
      onPressed: () => Navigator.of(context).pop(),
    );
    return NavBarHelpers.dialogue(context, 'Contact Us', [info, close]);
  }

  static void closeKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  static _navigateMain(context, targetRoute, onReturnCallback) {
    return () => Navigator.pushNamed(
      context,
      targetRoute,
    ).then((e) => onReturnCallback());
  }
}
