import 'package:flutter/material.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/screens/partials/nav_bar/profile_bar/profile_settings.dart';

class AccountScreenBar with NavBarHelpers {
   static AppBar bar(BuildContext context) {
     return AppBar(
      automaticallyImplyLeading: false,
      elevation: 5,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          NavBarHelpers.backButton(context),
          NavBarHelpers.logoIcon(context: context),
          ProfileSettings(),
        ],
      ),
    );
  }
}


 