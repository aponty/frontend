import 'package:flutter/material.dart';
import 'package:spot_catch/services/ad_manager.dart';

class MatchModal {
  static congratulate(BuildContext context) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {

        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          AdManager.showInterstitialAd();
        });

        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title : Text(
            "You got a match!",
            textAlign: TextAlign.center,
          ),
        );
      },
    );
  }
}
