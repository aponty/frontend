import 'package:flutter/material.dart';
import '../../models/user.dart';
import 'package:flip_card/flip_card.dart';
import '../partials/profile/profile.dart';

class ProfileCards extends StatelessWidget{
  final bool hasData;
  final List<User> users;
  final double barSize;

  ProfileCards(this.hasData, this.users, this.barSize);

  SizedBox centeredComponent(Widget component, BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Center(child: component),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!hasData) {
      return centeredComponent(CircularProgressIndicator(), context);
    } else if (users.length == 0) {
      String nobody = "There are no more users in this area; please try again later!";
      return centeredComponent(Text(nobody, textAlign: TextAlign.center), context);
    } else {
      return Stack( 
        children: users
          .map((usr) {
            return FlipCard(
              direction: FlipDirection.VERTICAL,
              key: usr.cardKey,
              flipOnTouch: false,
              front: Profile(UniqueKey(), usr, barSize, makeSpaceForLikeButtons: true),
              back: Container(),
            );
          }).toList(),
      );
    }
  }
}


  