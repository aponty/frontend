import 'package:flutter/material.dart';
import 'package:spot_catch/screens/home_screen/match_modal.dart';
import 'package:spot_catch/models/user.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/services/user_config_helpers.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/api/endpoints.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/services/routes.dart';
import 'package:flutter/scheduler.dart';
import 'dart:convert';
import './profile_card.dart';
import 'package:spot_catch/services/ad_manager.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool hasData = false;
  List<User> users;
  int animationDuration = 500;
  bool hasNewMatch = false;
  bool serverNotifications = false;
  int swipeAdCounter = 0;

  @override
  void initState() {
    _initialize();
    super.initState();
  }

  void _initialize(){
    AdManager.initialize();
    if (globals.isLoggedIn) {
      loadUsersAndNotifications();
    } else {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushNamed(context, AuthRoute).then((e) {
          loadUsersAndNotifications();
        });
      });
    }
  }

  @override
  void setState(fn) {
    if(mounted) super.setState(fn);
  }

  // three calls -> one call will be a lot faster...or all via websocket?
  void loadUsersAndNotifications() async{
    setState(()=> hasNewMatch = false);
    UserConfigHelpers.postConfigData(homeCallback: handlePushNotification);
    loadNotifications();
    loadUsers();
  }
  
  void loadNotifications() async {
    bool notifications = await User.hasNotifications();
    notifications ??= false;
    setState(()=> serverNotifications = notifications);
  }

  void handlePushNotification(Map<String, dynamic> message) {
    setState(()=> serverNotifications = true);
  }

  void loadUsers() async {
    List<User> loadedUsers = await User.fetchUnswiped();
    if (loadedUsers != null) {
      setState((){
        users = loadedUsers;
        hasData = true;
      });
    }
  }

  void updateUserList() async {
    if(users.length != 0) users.removeLast();
    if(users.length == 0){
      setState(() { hasData = false;});
      List<User> newUsers = await User.fetchUnswiped();
      setState(() { 
        users = newUsers;
        hasData = true;
      });
    } else {
      setState(() => users = users);
    }
  }

  void like() async {
    if (users.length == 0) return;
    int appUserID = globals.userID;
    Map<String, String> data = {
      'id': appUserID.toString(),
      'match_user_id': users.last.userID.toString(),
    };
    users.last.cardKey.currentState.toggleCard();
    List res = await Future.wait([
      Future.delayed(Duration(milliseconds: animationDuration),() => null),
      HTTPMethods.upload(data, MATCHES_ROOT, 'post'),
    ]);
    if (json.decode(res[1].body)['new_mutual_match']) {
      setState(() => hasNewMatch = true);
      MatchModal.congratulate(context);
    }
    updateUserList();
  }

  void dislike() async {
    if (users.length == 0) return;
    int appUserID = globals.userID;
    Map<String, String> data = {
      'id': appUserID.toString(),
      'match_user_id': users.last.userID.toString(),
    };
    users.last.cardKey.currentState.toggleCard();
    await Future.wait([
      Future.delayed(Duration(milliseconds: animationDuration),(){}),
      HTTPMethods.upload(data, MATCHES_DISLIKE, 'patch'),
    ]);
    updateUserList();
  }

  void _adCheck(double barSize) {
    setState(() => swipeAdCounter += 1);
    if (swipeAdCounter >= 3) {
      AdManager.showBannerAd(barSize, context);
      setState(() => swipeAdCounter = 0);
    }
  }

  IconButton button(callback, type, barSize, context) {
    return IconButton(
      iconSize: 55,
      icon: Icon(type, color: Colors.black),
      onPressed: () {
        _adCheck(barSize);
        callback();
      }
    );
  }

  Widget likeDislikeButtons(barSize, context){
    return Positioned(
      bottom: 0,
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            button(dislike, Icons.cancel, barSize, context),
            button(like, Icons.check_circle, barSize, context),
          ],
        ),
      ),
    );
  }

  bool hasNotifications (){
    return serverNotifications || hasNewMatch;
  }
  
  @override
  Widget build(BuildContext context) {
    globals.currentRoute = HomeRoute;
    AppBar appBar = NavBarHelpers.navBarMain(context, hasNotifications(), _initialize);
    double barSize = appBar.preferredSize.height;
    return Scaffold(
      appBar: appBar,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          hasData
          ? ProfileCards(hasData, users, barSize)
          : Center(child: CircularProgressIndicator()),
          likeDislikeButtons(barSize, context),
        ],
      ),
    );
  }
}
