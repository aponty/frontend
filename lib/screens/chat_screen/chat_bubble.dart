import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:spot_catch/services/linkifier.dart';

class ChatBubble extends StatelessWidget {
  final String message;
  final DateTime time;
  final bool seen, isMe;
  final IconData icon;
  final Color color;
  final MainAxisAlignment mainAlign;
  final CrossAxisAlignment crossAlign;
  final CircleAvatar matchIcon;
  final double minWidth = 75.0;
  ChatBubble({this.message, this.time, this.seen, this.isMe, this.matchIcon}) :
    icon = seen ? Icons.done_all : Icons.done,
    mainAlign = isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
    crossAlign = isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
    color = isMe ? Colors.black54 :  Colors.black;

  Widget buildTimestampRow(context) {
    DateTime localTime = time.toLocal();
    Text timeStamp = Text(
      DateFormat('M/d h:mm').format(localTime),
      style: TextStyle(
        color: Colors.black38,
        fontSize: 10.0,
      ),
    );

    return Container(
      constraints: BoxConstraints( maxWidth: minWidth ),
      padding: EdgeInsets.all(4),
      child: Row(
        children:[
          isMe ? Spacer() : Container(),
          timeStamp,
          SizedBox(width: 3.0),
          Icon(
            icon,
            size: 12.0,
            color: Colors.black38,
          ),
          isMe ? Container() : Spacer(),
        ]
      ),
    );
  }

  Widget userAvatar() {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 10),
      child: matchIcon
    );
  }
  @override
  Widget build(BuildContext context) {
    final radius = isMe
      ? BorderRadius.only(
          topLeft: Radius.circular(17.0),
          bottomLeft: Radius.circular(17.0),
          bottomRight: Radius.circular(17.0),
        )
      : BorderRadius.only(
          topRight: Radius.circular(17.0),
          bottomLeft: Radius.circular(17.0),
          bottomRight: Radius.circular(17.0),
      );
  
    return Row(
      mainAxisAlignment: mainAlign,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        isMe ? Container() : userAvatar(),
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 0.8,
            minWidth: minWidth,
          ),
          margin: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
            right: 10.0,
          ),
          padding: EdgeInsets.only(
            top: 8.0,
            right: 8.0,
            left: 8.0,
          ),
          decoration: BoxDecoration(
            // boxshadow here if you change the
            color: Colors.white,
            border: Border.all(
              color: color,
            ),
            borderRadius: radius,
          ),
          child: Column(
            crossAxisAlignment: crossAlign,
            children: <Widget>[
              Linkifier(message),
              buildTimestampRow(context),
            ],
          )
        )
      ],
    );
  }
}
