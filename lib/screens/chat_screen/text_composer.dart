import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Widget textComposer(context, controller, callback) {
    return Container(
          margin: EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: <Widget>[
             Flexible(
                child: TextField(
                  controller: controller,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration.collapsed(hintText: "Send a message"),
                ),
              ),
             Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: sendButton(context, callback)
              ),
            ],
          ),
    );
  }

  Widget sendButton(context, callback){
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return CupertinoButton(
        child: Text("Send"),
        onPressed: callback,
      );
    } else {
      return IconButton(
        icon: Icon(Icons.send),
        onPressed: callback,
      );
    }
  }
