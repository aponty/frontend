import 'package:flutter/material.dart';
import '../../models/user.dart';
import '../../models/message.dart';
import '../partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/api/auth/auth_helpers.dart';
import 'package:action_cable/action_cable.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import './text_composer.dart';
import './chat_bubble.dart';

class ChatScreen extends StatefulWidget {
  final User match;
  ChatScreen(this.match);
  @override
  createState() => _ChatScreenState(match);
}

class _ChatScreenState extends State<ChatScreen>{
  ActionCable cable;
  final User match;
  TextEditingController _controller = TextEditingController();
  List<Message> messages;
  bool connected;
  int connectionAttempt = 0;

  _ChatScreenState(this.match) 
  : messages = match.messages;

 @override
  void dispose() {
    super.dispose();
    cable.disconnect();
  }

  @override
  void initState() { 
    super.initState();
    connectChat();
  }

  void connectChat() async {
    Map<String, String> authHeader = await AuthHelpers.authTokenHeader();
    cable = ActionCable.Connect(
      Router.chatSocketRoute(),
      headers: authHeader,
      onConnected: (){
        setState(() => connected = true);
        subscribe();
      },
      onConnectionLost: () {
        print("connection lost; connection attempt: $connectionAttempt");
        if (connectionAttempt < 10) {
          setState((){
            connected = null;
            connectionAttempt = connectionAttempt + 1;
          });
          connectChat();
        } else {
          setState(() => connected = false);
        }
      }, 
      onCannotConnect: () {
        setState(() => connected = false);
        print("cannot connect");
      }
    );
  }

  void subscribe() {
    cable.subscribe(
      'ChatChannel',
      channelParams: { 
        'user_id' : globals.userID,
        'match_id' : match.userID,
         },
      onSubscribed: () => print('subscribtion message recieved'),
      onDisconnected: () {
        setState(() => connected = false);
        print('disconnect message recieved from rails');
      },
      onMessage: (Map message) {
        Message mess = Message.fromMap(message);
        messages.insert(0, mess);
        setState(() => messages = messages);
      },
    );
  }

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      cable.performAction(
        'ChatChannel', 
        action: 'speak',
        channelParams: { 
          'user_id' : globals.userID,
          'match_id' : match.userID,
        },
        actionParams: { 'message': _controller.text }
      );
      _controller.clear();
    }
  }

  ListView listViewOfMessages(){
    return ListView(
      reverse: true,
      children: messages.map((message) {
        return message.body == null 
          ? Container()
          : ChatBubble(
          message: message.body,
          isMe: message.sentByCurrentUser(),
          time: message.createdAt,
          seen: message.seenById(match.userID),
          matchIcon: match.icon(15.0),
          );
      }).toList(),
    );
  }

  Flexible centeredComponent(Widget component) {
    return Flexible(child: Center(child: component));
  }

  Widget mainWidg(){
    if (connected == null) {
      return centeredComponent(CircularProgressIndicator());
    }
    if (connected) {
      return Flexible(child: listViewOfMessages());
    } else {
      String mess = "We're having some issues connecting \n Please try again later :(";
      Text text = Text(mess, textAlign: TextAlign.center);
      return centeredComponent(text);
    }
  }

  @override
  Widget build(BuildContext context) {
    globals.currentRoute = ChatRoute;
    return Scaffold(
      appBar: NavBarHelpers.navBarChat(context, match.icon(30.0), match),
      body: Container(
        child: Column(
          children: <Widget>[
            mainWidg(),
            Divider(height: 1.0),
            textComposer(context, _controller, _sendMessage),
          ],
        ),
      ),
    );
  }
}
