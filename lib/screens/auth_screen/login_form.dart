import 'package:flutter/material.dart';
import './form_fields.dart';
import '../../services/api/auth/auth_helpers.dart';

class LoginForm extends StatefulWidget {
  final Function toggleLogin;
  LoginForm(this.toggleLogin);

  @override
  LoginFormState createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  bool termsChecked = false;
  bool validateLegal = false;

  void togglePswdVis() => setState(() => _obscureText = !_obscureText);
  void toggleLegal() => setState(()=> termsChecked = !termsChecked);

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void login() async {
    setState(() => validateLegal = true);
    if (formKey.currentState.validate() && termsChecked) {
      final String email = emailController.text.trim();
      final String password = passwordController.text;
      AuthHelpers.requestLogin(context, email, password, widget.toggleLogin);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Text('Log In',
              style: Theme.of(context).textTheme.headline4,
            ),
            FormFields.emailField(emailController),
            FormFields.passwordField(passwordController, _obscureText, togglePswdVis, false),
            FormFields.legalAgreement(context, toggleLegal, termsChecked, validateLegal),
            FormFields.submitButton(login),
            FormFields.loginSignUpToggle(widget.toggleLogin, 'login'),
            FormFields.forgotPassword(context),
          ],
        ),
    );
  }
}
