import 'package:flutter/material.dart';
import './form_fields.dart';
import '../../services/api/auth/auth_helpers.dart';

class SignUpForm extends StatefulWidget {
  final Function toggleLogin;
  SignUpForm(this.toggleLogin);

  @override
  SignUpFormState createState() => SignUpFormState();
}

class SignUpFormState extends State<SignUpForm> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool obscureText = true;
  bool termsChecked = false;
  bool validateLegal = false;
  int age = 10;
  String gender = "More/Other";

  void togglePswdVis() => setState(() => obscureText = !obscureText);
  void toggleLegal() => setState(()=> termsChecked = !termsChecked);
  void setAge(val) => setState(() => age = val);
  void setGender(val) => setState(() => gender = val);

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confController.dispose();
    super.dispose();
  }

  void signUp() async {
    setState(() => validateLegal = true);
    if (formKey.currentState.validate() && termsChecked) {
      Map<String, String> data = {
        'name': nameController.text.trim(),
        'email': emailController.text.trim(),
        'password': passwordController.text,
        'password_confirmation': passwordController.text,
        'age': age.toString(),
        'gender': gender,
      };
      AuthHelpers.requestSignUp(context, data, widget.toggleLogin);
    } 
  }
  
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: <Widget>[
          Text('Sign Up',
            style: Theme.of(context).textTheme.headline4,
          ),
          FormFields.nameField(nameController),
          FormFields.age(age, setAge),
          FormFields.gender(gender, setGender),
          FormFields.emailField(emailController),
          FormFields.passwordField(passwordController, obscureText, togglePswdVis, true),
          FormFields.passwordConfirmationField(confController, passwordController, obscureText, togglePswdVis),
          FormFields.legalAgreement(context, toggleLegal, termsChecked, validateLegal),
          FormFields.submitButton(signUp),
          FormFields.loginSignUpToggle(widget.toggleLogin, 'signup'),
          FormFields.forgotPassword(context),
        ],
      ),
    );
  }
}
