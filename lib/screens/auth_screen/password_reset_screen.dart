import 'package:flutter/material.dart';
import './form_fields.dart';
import '../../services/api/auth/auth_helpers.dart';
import '../partials/nav_bar/nav_bar.dart';

class PasswordResetScreen extends StatefulWidget {
  @override
  _PasswordResetScreenState createState() => _PasswordResetScreenState();
}

class _PasswordResetScreenState extends State<PasswordResetScreen> {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();

  void submit() async {
    final String email = emailController.text.trim();
    AuthHelpers.requestReset(context, email);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavBarHelpers.navBarBack(context),
      body: Builder(
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(40.0),
                  child:  Form(
                    key: formKey,
                    child: Column(
                      children: <Widget>[
                        Text('Reset Password',
                          style: TextStyle(
                            fontSize: 32.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(height: 75),
                        Text("Please enter your email here", textAlign: TextAlign.center),
                        Text("We'll send a reset link if we have an account for you", textAlign: TextAlign.center),
                        SizedBox(height: 25),
                        FormFields.emailField(emailController),
                        SizedBox(height: 25),
                        FormFields.submitButton(submit),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
