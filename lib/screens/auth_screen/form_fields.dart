import 'package:validators/validators.dart';
import 'package:flutter/material.dart';
import '../../services/routes.dart';

class FormFields {
  static final blackStyle = TextStyle(
        color: Colors.black,
        decorationColor: Colors.black,
      );

  static Widget inputField(TextEditingController controller) {
    return TextFormField(
      style: blackStyle,
      controller: controller,
      validator: (v) => v.isNotEmpty? null : '',
      decoration: InputDecoration(labelText: 'Send a message'),
    );
  }
  static Widget nameField(TextEditingController controller) {
    return TextFormField(
      style: blackStyle,
      cursorColor: Colors.black,
      controller: controller,
      validator: (v) => v.isNotEmpty? null : "Please enter your name",
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Name',
        hintText: 'Please enter your name',
      ),
    );
  }

  static Widget age(stateVal, stateCallback){
    List<int> ages = [for(var i=10; i<101; i+=1) i];
    return DropdownButtonFormField(
      value: stateVal,
      items: ages.map((val) {
        return DropdownMenuItem(
          value: val,
          child: Text(val.toString()),
        );
      }).toList(),
      onChanged: (val) => stateCallback(val),
      validator: (value) => value == 10 ? "Please enter your age" : null,
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Age',
      ),
    );
  }

  static Widget gender(stateVal, stateCallback){
    List<String> genders = [ 'Female', "Male", "More/Other" ];
    return DropdownButtonFormField(
      value: stateVal,
      items: genders.map((val) {
        return DropdownMenuItem(
          value: val,
          child: Text(val.toString()),
        );
      }).toList(),
      onChanged: (val) => stateCallback(val),
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Gender',
      ),
    );
  }

  static Widget emailField(TextEditingController controller) {
    return TextFormField(
      style: blackStyle,
      cursorColor: Colors.black,
      keyboardType: TextInputType.emailAddress,
      controller: controller,
      validator: (v) =>
          isEmail(v.trim()) ? null : "Please enter a valid email address",
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Email',
        hintText: 'you@example.com',
      ),
    );
  }

  static Widget legalAgreement(BuildContext context, Function toggleTermsVal, bool termsChecked, bool validate){
    Widget leading = RichText(
      text: TextSpan(
        text: "I agree with this application's ",
        style: TextStyle(
          color: Colors.black,
          fontStyle: FontStyle.italic,
        ),
        children: <TextSpan>[
          TextSpan(
            text: 'Terms and Conditions, EULA, and Privacy Policy',
            style: TextStyle(
              color: Colors.blueAccent, 
              decoration: TextDecoration.underline,
            ),
          )
        ]
      ),
    );
    
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: CheckboxListTile(
        title: InkWell(
          child: leading,
          onTap: ()=> Navigator.pushNamed(context, LegalRoute),
        ),
        value: termsChecked,
        onChanged: (bool val) => toggleTermsVal(),
        subtitle: (!termsChecked && validate)
          ? Padding(
              padding: EdgeInsets.fromLTRB(12.0, 0, 0, 0), 
              child: Text(
                'Required field', 
                style: TextStyle(
                  color: Color(0xFFe53935), 
                  fontSize: 12,
                ),
              ),
            )
          : null,
      ),
    );
  }

  static Widget passwordField(controller, obscureText, toggle, validate) {
    Function validator = (String val) => (val.length < 6 && validate) ? 'Password too short.' : null;

    return TextFormField(
      controller: controller,
      validator: validator,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Password',
        suffixIcon: IconButton(
          onPressed: toggle,
          icon: Icon(obscureText ? Icons.visibility : Icons.visibility_off, color: Colors.black),
        ),
      ),
    );
  }

  static Widget passwordConfirmationField(
      controller, passwordController, obscureText, toggle) {
    return TextFormField(
      controller: controller,
      validator: (val) =>
          val != passwordController.text ? 'Passwords do not match' : null,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelStyle: blackStyle,
        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 7.0)),
        labelText: 'Please enter your password again',
      ),
    );
  }

  static Widget submitButton(Function callback) {
    return Container(
      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
      child: OutlineButton(
        borderSide: BorderSide(color: Colors.black),
        shape: StadiumBorder(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Submit'),
          ],
        ),
        onPressed: () => callback(),
      ),
    );
  }

  static Widget loginSignUpToggle(Function callback, String currentForm) {
    final String leadingText = currentForm == 'signup'
        ? "Already have an account?"
        : "Don't have an account?";
    final String trailingText = currentForm == 'signup' ? "Log In" : "Sign Up";

    return Container(
      margin: EdgeInsets.fromLTRB(5, 10, 5, 0),
      child: OutlineButton(
        borderSide: BorderSide(color: Colors.black),
        shape: StadiumBorder(),
        onPressed: callback,
        child: Row(
          children: <Widget>[
            Text(leadingText),
            Spacer(),
            Text(
              trailingText,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  static Widget forgotPassword(context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: InkWell(
        child: Text('Forgot Password?'),
        onTap: ()=> Navigator.pushNamed(context, PasswordResetRoute),
      ),
    );
  }
}
