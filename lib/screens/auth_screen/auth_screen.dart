import 'package:flutter/material.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/screens/auth_screen/login_form.dart';
import 'package:spot_catch/screens/auth_screen/signup_form.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  bool showSignUp = true;

  toggleSignUpLogin() {
    setState(() {
      showSignUp = !showSignUp;
    });
  }

  @override
  Widget build(BuildContext context) {
    globals.currentRoute = AuthRoute;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ImageIcon(
                    AssetImage("assets/images/branding/v_lockup_trans.png"),
                    color: Colors.black,
                    size: 200,
                  ),
                  showSignUp
                      ? SignUpForm(toggleSignUpLogin)
                      : LoginForm(toggleSignUpLogin),
                  ],
                ),
              ),
            ), 
        ),
      )
    );
  }
}
