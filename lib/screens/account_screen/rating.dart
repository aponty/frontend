// pretty sure you can just delete all this

// import 'package:flutter/material.dart';

// class Rating extends StatefulWidget {
//   final String endpoint;
//   final int currentVal;
//   final int maxRange;
//   final Widget selectedIcon;
//   final Widget unselectedIcon;
//   final Function callback;
//   final String route;
//   final String method;

//   Rating({
//     Key key,
//     this.endpoint = '',
//     this.currentVal = 0,
//     this.maxRange = 5,
//     this.callback,
//     this.route,
//     this.method,
//     this.selectedIcon= const Icon(Icons.star),
//     this.unselectedIcon= const Icon(Icons.star_border),
//     }) : super(key: key);

//   @override
//   _RatingState createState() => _RatingState(currentVal, maxRange, callback, route, method);
// }

// class _RatingState extends State<Rating> {
//   int val;
//   int max;
//   Function callback;
//   String route;
//   String method;
//   List<String> lables = [
//     'Screw it, I wear my helmet at the gym',
//     'Equalize those bolts just to be safe',
//     'AMGA approved, or pretty closeish',
//     'No really, speed is safe! ...and fun',
//     'Honnold called me sketchy once'
//   ];

//   _RatingState(this.val, this.max, this.callback, this.route, this.method);

//   Widget _divider(percent, color) {
//     return Container(
//       decoration: BoxDecoration(
//         border: Border(
//           top: BorderSide(
//             color: color
//           ),
//         ),
//       ),
//       width: MediaQuery.of(context).size.width * percent,
//     );
//   }
//   List<Column> _icons() {
//     return List.generate(max, (index) {
//         return Column(
//           children: <Widget>[
//             _divider(0.8, Colors.black12),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 Expanded(
//                   child: IconButton(
//                     onPressed: (){
//                       setState((){ val = index;});
//                       callback({'sketch_rating': val.toString()}, route, method);
//                     },
//                     iconSize: 48,
//                     icon: index <= val ? widget.selectedIcon : widget.unselectedIcon
//                   ),
//                 ),
//                 Expanded(
//                   child: Text(lables[index]),
//                   flex: 2,
//                 ),
//               ],
//             ),
//           ],
//         );
//       });
//   }

//   // Widget _header(){
//   //   const leadingStyle = TextStyle(
//   //     fontSize: 20.0,
//   //     color: Colors.black,
//   //   );

//   //   const subStyle = TextStyle(
//   //     fontSize: 16.0,
//   //     color: Colors.black,
//   //   );

//   //   return Row(
//   //     mainAxisAlignment: MainAxisAlignment.center,
//   //     children: <Widget>[
//   //       Column(
//   //         children: <Widget>[
//   //           SizedBox(height: 10),
//   //           Text('Climbing can involve risk.', style: leadingStyle),
//   //           Text('How spicy do you like it?', style: subStyle),
//   //           SizedBox(height: 10),
//   //         ],
//   //       )
//   //     ],
//   //   );
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: MediaQuery.of(context).size.width * 0.9,
//       child: Column(
//         children: <Widget>[
//           SizedBox(height: 10),
//           ..._icons(),
//           _divider(0.8, Colors.black12),
//           SizedBox(height: 10),
//         ],
//       ),
//     );
//   }
// }
