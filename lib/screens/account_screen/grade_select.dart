import 'package:flutter/material.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/api/endpoints.dart';

Widget gradeSelect(type, callback, user, context) {

  List<String> boulderingGrades = ["N/A"] + Iterable<int>.generate(17)
    .map((v) => "V" + v.toString()).toList();

  List<String> routeGrades = ["N/A"] + Iterable<int>.generate(16)
      .map((v) => "5." + v.toString()).toList();

    Map<String, Map<String, dynamic>> options = {
      'Bouldering': {
        'key': 'boulder_grade',
        'grades': boulderingGrades,
        'stateVar': user.boulderingGrade,
      },
      'Top Rope': {
        'key': 'tr_grade',
        'grades': routeGrades,
        'stateVar': user.trGrade,
      },
      'Sport': {
        'key': 'sport_grade',
        'grades': routeGrades,
        'stateVar': user.sportGrade,
      },
      'Trad': {
        'key': 'trad_grade',
        'grades': routeGrades,
        'stateVar': user.tradGrade,
      },
    };

    List<String> items = options[type]['grades'];
    String stateVar = options[type]['stateVar'];

    return Column(
      children: <Widget>[
        Text(type, style: TextStyle(fontSize: 12)),
        Container(
            width: MediaQuery.of(context).size.width * 0.35,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black54),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: true,
                  child: DropdownButton(
                    hint: Text(type),
                    value: stateVar,
                    items: items.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (val) {
                      HTTPMethods.upload(
                        {options[type]['key']: val},
                        USERS_ROOT + user.userID.toString(),
                        'patch',
                      );
                      callback(type, val);
                    },
                  ),
              ),
            ),
        ),
      ],
    );
  }
