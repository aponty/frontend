import 'package:flutter/material.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/screens/partials/nav_bar/profile_bar/profile_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:spot_catch/models/user.dart';
import 'package:spot_catch/screens/account_screen/sketch_rating.dart';
import 'package:spot_catch/screens/account_screen/grade_select.dart';
import 'package:spot_catch/models/image.dart';
import 'package:spot_catch/services/api/endpoints.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  TextEditingController _descriptionController;
  TextEditingController _nameController;
  bool hasData = false;
  int userID;
  User user;
  TextStyle sectionHeader = TextStyle(fontSize: 20.0);

  @override
  void initState() {
    super.initState();
    loadUser();
  }

  @override
  void dispose() {
    if (mounted) {
      super.dispose();
      _descriptionController.dispose();
      _nameController.dispose();
    }
  }

  loadUser() async {
    User thisUser = await User.fetchUser(globals.userID);
    String description = thisUser.description;

    setState(() {
      userID = globals.userID;
      user = thisUser;
      _descriptionController = TextEditingController(text: description);
      _nameController = TextEditingController(text: thisUser.name);
      hasData = true;
    });
  }

  Future _selectPicture(imageKey) async {
    final image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 500, 
      maxWidth: 500,
    );

    if (image != null){
      Img uploaded = await Img.uploadImage(image, imageKey);
      setState(() => user.images[imageKey] = uploaded);
    }
  }

  Widget _imageUploadWidget(imageKey) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Material(
          color: Colors.grey,
          child: InkWell(
            onTap: () => _selectPicture(imageKey),
            child: Container(
              height: 100.0,
              width: 100.0,
              child: Icon(Icons.add_a_photo),
            ),
          ),
        ),
      ),
    );
  }

  void _deleteImage(imageKey) async {
    Img image = user.images[imageKey];
    bool success = await Img.deleteImage(image);
    if(success) setState(() => user.images[imageKey] = Img.fromMap({}));
  }

  Widget _savedImageWidget(imageKey) {
    Image image = user.images[imageKey].image;
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width * .9,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: image,
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: ClipOval(
            child: Container(
              color: Colors.white54,
              child: IconButton(
                iconSize: 40,
                icon: Icon(Icons.cancel),
                color: Colors.black,
                onPressed: () => _deleteImage(imageKey),
              ), 
            ),
          ) ,
        )
      ],
    );   
  }

  void updateGradeState(type, val) {
    if (type == "Bouldering") {
      setState(() => user.boulderingGrade = val); 
    } else if (type == "Top Rope") {
       setState(() => user.trGrade = val);
    } else if (type == "Sport") {
       setState(() => user.sportGrade = val);
    } else if (type == "Trad") {
       setState(() => user.tradGrade = val);
    }
  }

  Widget _formFieldLabel(lable, {trailing}){
    return Column(
      children: [
        SizedBox(height: 10),
        _divider(),
        SizedBox(height: 5),
        Container(
          width: MediaQuery.of(context).size.width * 0.9,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                lable,
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 17),
              ),
              trailing != null ? 
                Expanded(
                  child: Text(
                    trailing,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 15),
                  ),
                )
                : Container(),
            ],
          )
        ),
      ],
    );
  }

  Widget _divider(){
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.black54
          ),
        ),
      ),
      width: MediaQuery.of(context).size.width * 0.9,
    );
  }

  Widget _gradeSelectionSection(){
    return Column(
      children: <Widget>[
        SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: <Widget>[
                gradeSelect('Bouldering', updateGradeState, user, context),
                SizedBox(height: 15),
                gradeSelect('Trad', updateGradeState, user, context),
              ],
            ),
            Column(
              children: <Widget>[
                gradeSelect('Sport', updateGradeState, user, context),
                SizedBox(height: 15),
                gradeSelect('Top Rope', updateGradeState, user, context),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _gender(){
    List<String> genders = [ 'Female', "Male", "More" ];
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton(
            value: user.gender,
            items: genders.map((val) {
              return DropdownMenuItem(
                value: val,
                child: Text(val),
              );
            }).toList(),
            onChanged: (val) {
              setState(() => user.gender = val);
              HTTPMethods.upload(
                {'gender': val},
                USERS_ROOT + globals.userID.toString(), 
                'patch',
              );
            },
          )
        )
      )
    );
  }

  Widget _age(){
    List<int> ages = [for(var i=10; i<101; i+=1) i];
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton(
            value: user.age,
            items: ages.map((val) {
              return DropdownMenuItem(
                value: val,
                child: Text(val.toString()),
              );
            }).toList(),
            onChanged: (val) {
              setState(() => user.age = val);
              HTTPMethods.upload(
                {'age': val.toString()},
                USERS_ROOT + globals.userID.toString(), 
                'patch',
              );
            },
          )
        )
      )
    );
  }

  Widget _name(){
    return Padding(
      padding: EdgeInsets.fromLTRB(75, 15,75,0),
      child: TextField(
        onEditingComplete: (){
          HTTPMethods.upload({'name': _nameController.text},
            "/users/" + userID.toString(), 'patch');
          FocusScope.of(context).unfocus();
        },
        textAlign: TextAlign.center,
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        controller: _nameController,
        maxLength: 25,
        decoration: InputDecoration(
          counter: Container(),
          border: OutlineInputBorder(),
          hintText: 'Name...',
          labelStyle: TextStyle(
            color: Colors.black
          ),
          focusedBorder: OutlineInputBorder(),
        ),
      ),
    );
  }

  Widget _searchRadius(){
    return Slider(
      value: user.searchRadius,
      min: 0,
      max: 300,
      divisions: 300,
      activeColor: Colors.black54,
      label: user.searchRadius.round().toString(),
      inactiveColor: Colors.black26,
      onChanged: (double value) {
        setState(() => user.searchRadius = value);
      },
      onChangeEnd: (double value) {
        setState(() => user.searchRadius = value);

        HTTPMethods.upload(
          {'search_radius': user.searchRadius.toString()},
          USERS_ROOT + globals.userID.toString(), 
          'patch',
        );
      },
    );
  }

  Widget aboutMe(){
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            textAlign: TextAlign.center,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.text,
            textCapitalization: TextCapitalization.sentences,
            controller: _descriptionController,
            maxLines: null,
            onEditingComplete: (){
              HTTPMethods.upload({'description': _descriptionController.text},
                "/users/" + userID.toString(), 'patch');
              FocusScope.of(context).unfocus();
            },
            maxLength: 200,
            decoration: InputDecoration(
              counter: Container(),
              border: OutlineInputBorder(),
              hintText: 'Share my rope because...',
              labelStyle: TextStyle(
                color: Colors.black
              ),
              focusedBorder: OutlineInputBorder(),
            ),
          ),
        ),
      ],
    );
  }

  Widget imageSwitch(imageKey) {
    return (user.images[imageKey]?.id == null)
      ? _imageUploadWidget(imageKey)
      : _savedImageWidget(imageKey);
  }

  @override
  Widget build(BuildContext context) {
    globals.currentRoute = ProfileRoute;
    return Scaffold(
      appBar: ProfileBar.bar(context),
      body: !hasData 
        ? Center( child: CircularProgressIndicator())
        : GestureDetector(
            onTap: () {
              HTTPMethods.upload({
                'description': _descriptionController.text,
                'name': _nameController.text,
                },
                  "/users/" + userID.toString(), 'patch');
              FocusScope.of(context).unfocus();
            },
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    _formFieldLabel("Upload Pictures"),
                    imageSwitch('image_0'),
                    imageSwitch('image_1'),
                    imageSwitch('image_2'),
                    _formFieldLabel("Name"),
                    _name(),
                    _formFieldLabel("Search Distance (miles)", trailing: user.searchRadius.round().toString()),
                    _searchRadius(),
                    _formFieldLabel("Age"),
                    _age(),
                    _formFieldLabel("Gender"),
                    _gender(),
                    _formFieldLabel("Climbing Grades"),
                    _gradeSelectionSection(),
                    _formFieldLabel("Risk Tolerance", trailing: "How spicy do you like it?"),
                    SketchRating(
                      user.sketchRating, 
                      (val) => setState(() => user.sketchRating = val),
                    ),
                    _formFieldLabel("About Me"),
                    aboutMe(),
                  ],
                ),
              ),
            ),
          ),
    );
  }
}
