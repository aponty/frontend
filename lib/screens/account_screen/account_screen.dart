import 'package:flutter/material.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/screens/partials/nav_bar/account_screen_bar/account_screen_bar.dart';
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';

class AccountScreen extends StatelessWidget {

  Widget header(String text) {
    return Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black,
          decorationColor: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 17,
        ),
    );
  }

  Widget legal(context){
    return InkWell(
      child: SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.content_paste),
            SizedBox(width: 20),
            Text('Legal Information'),
          ],
        ),
      ),
      onTap: () => Navigator.pushNamed(context, LegalRoute),
    );
  }

  Widget contact(context){
    return InkWell(
      onTap: () => NavBarHelpers.contactUsDialogue(context),
      child: SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.email),
            SizedBox(width: 20),
            Text('Contact Us'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AccountScreenBar.bar(context) ,
      body:Column(
        children: [
          Flexible(
            flex: 6,
            child: InkWell(
              onTap: () => Navigator.pushNamed(context, ProfileRoute),
              child: Column(
                children: [
                  SizedBox(height: 50),
                  Icon(
                    Icons.account_circle,
                    size: 150,
                  ),
                  Text(
                    'Edit Profile',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    )
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                legal(context),
                SizedBox(width: 50),
                contact(context),
              ],
            ),
          ),
          Spacer(flex: 1)
        ],
      ),
    );
  }
}
