import 'package:flutter/material.dart';
import 'package:spot_catch/services/api/endpoints.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/services/api/http_methods.dart';

class SketchRating extends StatelessWidget {
  final int userSketchRating;
  final Function setUserState;
  SketchRating(this.userSketchRating, this.setUserState);

  final items =  [
    // not DRY, but was annoying making them constants out of a function
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ 
          Text('Screw that, I wear my helmet at the gym', style: TextStyle(fontSize: 14)),
          Text('🌶️', style: TextStyle(fontSize: 14)),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ 
          Text('Equalize those bolts just to be safe ', style: TextStyle(fontSize: 14)),
          Text('🌶️🌶️', style: TextStyle(fontSize: 14)),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ 
          Text('AMGA approved, or pretty closeish ', style: TextStyle(fontSize: 14)),
          Text('🌶️🌶️🌶️', style: TextStyle(fontSize: 14)),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ 
          Text('No really, speed is safe! ...and fun', style: TextStyle(fontSize: 14)),
          Text('🌶️🌶️🌶️🌶️', style: TextStyle(fontSize: 14)),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [ 
          Text('Honnold called me sketchy once', style: TextStyle(fontSize: 14)),
          Text('🌶️🌶️🌶️🌶️🌶️', style: TextStyle(fontSize: 14)),
        ],
      ),
    ];
  
 
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton(
              isExpanded: true,
              value: items[userSketchRating],
              items: items.map((blurb) {
                return DropdownMenuItem(
                  value: blurb,
                  child: blurb,
                );
              }).toList(),
              onChanged: (val) {
                int index = items.indexOf(val);
                setUserState(index);
                HTTPMethods.upload(
                  {'sketch_rating': index.toString() },
                  USERS_ROOT + globals.userID.toString(),
                  'patch',
                );
              },
            ),
        ),
      ),
    );
  }
}
