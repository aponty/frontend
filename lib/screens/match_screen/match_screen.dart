import 'package:flutter/material.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'package:spot_catch/screens/partials/nav_bar/nav_bar.dart';
import 'package:spot_catch/screens/partials/profile/profile.dart';
import 'package:spot_catch/models/user.dart';
import 'package:spot_catch/models/image.dart';
import 'package:flutter/scheduler.dart';

class MatchScreen extends StatefulWidget{
  final User match;
  MatchScreen(this.match);
  @override
  _MatchScreenState createState() => _MatchScreenState(match);
}

class _MatchScreenState extends State<MatchScreen> {
  final User match;
  _MatchScreenState(this.match);

  @override
  void initState(){
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback(
      (_) => fetchImages()
    );
  }
  
  void fetchImages() async {
    Map ims = await Img.loadImages(match.userID);
    setState(() => match.images = ims);
  }

  @override
  Widget build(BuildContext context) {
    globals.currentRoute = MatchRoute;
    AppBar appBar = NavBarHelpers.navBarMatch(context, match);
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Profile(UniqueKey(), match, appBar.preferredSize.height)
      ),
    );
  }
}
