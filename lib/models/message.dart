import '../services/globals.dart' as globals;
import '../services/api/http_methods.dart';
import 'dart:convert';

class Message {
  final int senderID;
  final int conversationID;
  final String body;
  final DateTime createdAt;
  final List seenBy;

  Message({
    this.senderID,
    this.conversationID,
    this.body,
    this.createdAt,
    this.seenBy,
  });

  factory Message.fromMap(map) {
    map['created_at'] ??= "20000000";
    return Message(
      body: map['body'],
      conversationID: map['conversation_id'],
      senderID: map['user_id'],
      createdAt: DateTime.parse(map['created_at']),
      seenBy: map['seen_by']
    );
  }

  static Future<List<Message>> fetchMessages(int userID, int matchID) async {
    String route = "/messages/${userID.toString()}/${matchID.toString()}";
    
    var data = await HTTPMethods.fetch(route);
    data = json.decode(data.body);
    List<Message> messages = data.map<Message>((message) => Message.fromMap(message)).toList();
    return messages;
  }

  bool seenById(int seerId) {
    if(seenBy != null && seenBy.contains(seerId)) return true;
    return false;
  }

  bool sentByCurrentUser(){
    return senderID == globals.userID;
  }
}
