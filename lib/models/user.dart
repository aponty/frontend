import 'package:flutter/material.dart';
import 'dart:convert';
import '../services/api/http_methods.dart';
import 'package:flip_card/flip_card.dart';
import '../models/message.dart';
import '../services/globals.dart' as globals;
import './image.dart';

class User {
  final int userID;
  final String name;
  int age;
  String gender;
  Map images;
  String boulderingGrade;
  String trGrade;
  String sportGrade;
  String tradGrade;
  String description;
  int sketchRating;
  GlobalKey<FlipCardState> cardKey;
  List<Message> messages;
  double distanceFrom;
  DateTime matchDate;
  bool beenReported;
  bool userHasSeen;
  double searchRadius;

  User({
    this.userID, 
    this.name,
    this.age,
    this.gender,
    this.description,
    this.images,
    this.boulderingGrade,
    this.trGrade,
    this.sportGrade,
    this.tradGrade,
    this.sketchRating,
    this.cardKey,
    this.messages, 
    this.distanceFrom,
    this.matchDate,
    this.beenReported,
    this.userHasSeen,
    this.searchRadius,
    });

  factory User.fromMap(Map<String, dynamic> map) {
    map['messages'] ??= [];
    if(map['messages'].isEmpty) map['messages'] = [{}];
    map['images'] ??= {};
    map['messages'] = map['messages'].map<Message>((mess) => Message.fromMap(mess)).toList();

    // I believe this is redundant (server-side) but also don't hate the double check
    map['messages'].sort((Message a, Message b) => b.createdAt.compareTo(a.createdAt));

    return User(
      userID: map['id'],
      name: map['name'] ?? "",
      age: map['age'],
      gender: map['gender'],
      images: map['images'].map((imKey, im) => MapEntry('$imKey', Img.fromMap(im))),
      sketchRating: map['sketch_rating'] ?? 3,
      boulderingGrade: map["boulder_grade"] ?? "V0",
      trGrade: map["tr_grade"] ?? "5.0",
      sportGrade: map["sport_grade"] ?? "5.0",
      tradGrade: map["trad_grade"] ?? "5.0",
      description: map["description"] ?? "",
      cardKey: GlobalKey<FlipCardState>(),
      // match attributes
      messages: map['messages'],
      distanceFrom: map['distance_from_user'],
      matchDate: map['match_date'] == null ? null : DateTime.parse(map['match_date']),
      beenReported: map['been_reported'] ?? false,
      userHasSeen: map['user_has_seen'],
      searchRadius: map['search_radius'],
    );
  }

  CircleAvatar icon(size) {
    size = size.toDouble();
    var matchIconImage = images['icon'] ?? {};
    return CircleAvatar(
      radius: size,
      backgroundImage: matchIconImage.image?.image,
      backgroundColor: Colors.black,
    );
  }
  
  DateTime recentDate(){
    bool hasMessage = messages.last.body != null;
    return hasMessage ? messages.last.createdAt : matchDate;
  }

  static List<User>_usersFromRes(res){
    if (res != null) {
      res = json.decode(res.body);
      return res.map<User>((user) => User.fromMap(user)).toList();
    }
    return null;
  }

  static Future<User> fetchUser(userID) async {
    String route = "/users/${userID.toString()}";
    var profileData = await HTTPMethods.fetch(route);
    if (profileData != null) {
      return User.fromMap(json.decode(profileData.body));
    }
    return null;
  }

  static Future<bool> hasNotifications() async {
    String id = globals.userID.toString();
    String route = "/notifications/$id";
    var data = await HTTPMethods.fetch(route);
    if (data != null) {
      return json.decode(data.body)['has_notifications'];
    }
    return null;
  }

  static Future<List<User>> fetchFiveUnswiped() async {
    String id = globals.userID.toString();
    String route = "/users/$id/closest_five_new";
    var data = await HTTPMethods.fetch(route);
    return _usersFromRes(data);
  }

  static Future<List<User>> fetchUnswiped() async {
    String id = globals.userID.toString();
    String route = "/users/$id/closest_new";
    var data = await HTTPMethods.fetch(route);
    return _usersFromRes(data);
  }

  static Future<List<User>> fetchMatches() async {
    String id = globals.userID.toString();
    String route = "/matches/$id";
    var data = await HTTPMethods.fetch(route);
    return _usersFromRes(data);
  }

  static Future<User> fetchMatch(matchId) async {
    String userId = globals.userID.toString();
    String route = "/matches/$userId/$matchId";
    var data = await HTTPMethods.fetch(route);
    if (data != null) {
      return User.fromMap(json.decode(data.body));
    }
    return null;
  }
}
