import 'package:flutter/material.dart';
import '../services/api/endpoints.dart';
import '../services/api/http_methods.dart';
import '../services/globals.dart' as globals;
import 'dart:convert';

class Img {
  final int id;
  final String base64Image;
  final Image image;
  final String imageKey;

  Img({this.id, this.base64Image, this.image, this.imageKey});

  factory Img.fromMap(map) {
    map ??= {};
    return Img(
      id: map['id'],
      base64Image: map['image'],
      imageKey: "image_${map['image_key']}",
      image: map['image'] == null ? null : Image.memory(base64Decode(map['image']), fit: BoxFit.fitWidth),
    );
  }
  
  static Future<Img> uploadImage(image, imageKey) async {
    String base64Image = base64Encode(image.readAsBytesSync());
    Map data = {
      "image": base64Image,
      "image_key" : imageKey,
      "user_id" : globals.userID.toString(),
    };
    var res = await HTTPMethods.upload(data, IMAGES_ROOT, 'post');

    return Img.fromMap({
      "id": int.parse(res.body),
      "image" : base64Image,
      'image_key' : imageKey,
    });
  }

  static Future<bool> deleteImage(image) async {
    String id = image.id.toString();
    return await HTTPMethods.delete('$IMAGES_ROOT/$id');
  }

  static Future<Map> loadImages(userId) async {
    String id = userId.toString();
    String route = "$PROFILE_IMAGES/$id";
    var data = await HTTPMethods.fetch(route);
    data = json.decode(data.body);
    return data.map((imKey, im) => MapEntry('$imKey', Img.fromMap(im)));
  }
}
