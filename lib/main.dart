import 'package:flutter/material.dart';
import 'package:spot_catch/services/routes.dart';
import 'package:spot_catch/services/api/auth/auth_helpers.dart';
import 'package:spot_catch/services/globals.dart' as globals;
import 'dart:async';
import 'package:spot_catch/services/api/http_methods.dart';
import 'package:spot_catch/services/api/endpoints.dart';
const devEnv = !globals.isProduction;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  globals.isLoggedIn = await AuthHelpers.hasToken();

  FlutterError.onError = (FlutterErrorDetails details) async {
    if (devEnv) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  runZonedGuarded<Future<Null>>(
    () async => runApp(MyApp()),
    (error, stackTrace) async {
      await _reportError(error, stackTrace);
    }
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Open_Sans',
        primaryColor: Colors.white,
        accentColor: Colors.black,
        textTheme: TextTheme(
          headline4: TextStyle(
            fontFamily: "Lato_Black",
            fontStyle: FontStyle.normal,
            color: Colors.black,
          ),
        ),
      ),
      navigatorKey: globals.navigatorKey,
      onGenerateRoute: Router.generateRoute,
      initialRoute: HomeRoute,
    );
  }
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  print('Caught error: $error');
  print(stackTrace);
  if (devEnv) return;
  print('posting error...');
  Map<String, String> data = {
    "error": error.toString(),
    "stacktrace": stackTrace.toString(),
  };
  HTTPMethods.upload(data, ERROR_LOG, 'post');
}
